﻿namespace SeleniumHubNUnit.Dto
{
    public class SeleniumProfile
    {
        public string ProfileName { get; set; }
        public string BrowserName { get; set; }
        public string BrowserVersion { get; set; }
        public string Os { get; set; }
        public string OsVersion { get; set; }
        public string Resolution { get; set; }
    }
}
