﻿using System.Collections.Generic;

namespace SeleniumHubNUnit.Dto
{
    public class SeleniumConfig
    {
        public string Provider { get; set; }  
        public string Server { get; set; }
        public string User { get; set; }
        public string Password { get; set; }
        public List<SeleniumProfile> Profiles { get; set; }
        public SeleniumProfile Profile { get; set; }
    }
}
