﻿using System;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using SeleniumHubNUnit.Dto;
using System.Linq;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Edge;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Safari;
using SeleniumHubNUnit.Support;

namespace SeleniumHubNUnit.Test
{
    [TestFixture]
    public class TestBase
    {
        protected IWebDriver driver;
        private readonly string nombrePerfil;

        public TestBase(string nombrePerfil)
        {
            this.nombrePerfil = nombrePerfil;
        }

        [SetUp]
        public void Init()
        {
            var directorioBase = Environment.CurrentDirectory;
            var directorio = directorioBase + "//config.json";
            Console.WriteLine("Leyendo configuraciond desde directorio : " + directorio);

            var configuracion = JObject.Parse(File.ReadAllText(directorio));

            var json = configuracion.ToString();
            var config = JsonConvert.DeserializeObject<SeleniumConfig>(json);

            config.Profile = !string.IsNullOrEmpty(nombrePerfil)
                ? config.Profiles.FirstOrDefault(x => x.ProfileName.Equals(nombrePerfil))
                : config.Profiles.FirstOrDefault();

            Console.WriteLine("Perfil seleccionado: " + config.Profile?.ProfileName);

            SobreescribirParametros(config);

            
            Console.WriteLine("Proveedor de SeleniumGrid : " + config.Provider);

            var capabilities = CargarCapabilities(config);
            var remoteUrl = $"{config.Server}/wd/hub/";

            Console.WriteLine("Inicializando Selenium Grid Remoto : " + remoteUrl);
            driver = new RemoteWebDriver(new Uri(remoteUrl), capabilities);
        }

        [TearDown]
        public void Cleanup()
        {
            driver?.Quit();
        }




        private static ICapabilities CargarCapabilities(SeleniumConfig config)
        {
            ICapabilities capabilities = null;

            switch (config.Provider)
            {
                case TestConstantes.Standalone:
                {
                    var nombreBrowser = config.Profile?.BrowserName.ToLower();

                    switch (nombreBrowser)
                    {
                        case TestConstantes.GoogleChrome:
                            capabilities = new ChromeOptions().ToCapabilities();
                            break;
                        case TestConstantes.Firefox:
                            capabilities = new FirefoxOptions().ToCapabilities();
                            break;
                        case TestConstantes.Safari:
                            capabilities = new SafariOptions().ToCapabilities();
                            break;
                        case TestConstantes.InternetExplorer:
                            capabilities = new InternetExplorerOptions().ToCapabilities();
                            break;
                        case TestConstantes.Edge:
                            capabilities = new EdgeOptions().ToCapabilities();
                            break;
                    }

                    break;
                }

                case TestConstantes.BrowserStack:
                {
                    var bsCapabilities = new DesiredCapabilities(); //Deprecado

                    bsCapabilities.SetCapability("browser", config.Profile?.BrowserName);
                    bsCapabilities.SetCapability("browser_version", config.Profile?.BrowserVersion);
                    bsCapabilities.SetCapability("os", config.Profile?.Os);
                    bsCapabilities.SetCapability("os_version", config.Profile?.OsVersion);
                    bsCapabilities.SetCapability("resolution", config.Profile?.Resolution);
                    bsCapabilities.SetCapability("browserstack.user", config.User);
                    bsCapabilities.SetCapability("browserstack.key", config.Password);

                    capabilities = bsCapabilities;
                    break;
                }

                case TestConstantes.AmazonWs:
                {
                    //todo falta probar...
                    break;
                }
            }

            return capabilities;
        }

        public static void SobreescribirParametros(SeleniumConfig config)
        {
            // --params:browserName=Firefox,browserVersion=68.0

            var overrideBrowserName = TestContext.Parameters.Get("browserName");
            if (!string.IsNullOrEmpty(overrideBrowserName))
            {
                config.Profile.BrowserName = overrideBrowserName;
                Console.WriteLine("Sobre-escribiendo parametro browserName a: " + overrideBrowserName);
            }
                

            var overrideBrowserVersion = TestContext.Parameters.Get("browserVersion");
            if (!string.IsNullOrEmpty(overrideBrowserVersion))
            {
                config.Profile.BrowserVersion = overrideBrowserVersion;
                Console.WriteLine("Sobre-escribiendo parametro browserVersion a: " + overrideBrowserName);
            }

        }



       

    }
}
