﻿using NUnit.Framework;
using OpenQA.Selenium;

namespace SeleniumHubNUnit.Test
{
    [TestFixture("win10chrome1024x768")]
    [TestFixture("win10firefox1024x768")]
    //[TestFixture("win10iexplore1024x768")]
    [TestFixture("macsafari1024x768")]
    public class GmailTestBase : TestBase
    {
        public GmailTestBase(string nombrePerfil)
            : base(nombrePerfil)
        {

        }

        [Test]
        public void IniciarSesionGmail()
        {
            driver.Navigate().GoToUrl(
                "https://accounts.google.com/AccountChooser?service=mail&amp;continue=https://mail.google.com/mail/");
            driver.Manage().Window.Maximize();

            driver.FindElement(By.Id("identifierId")).Click();
            driver.FindElement(By.Id("identifierId")).SendKeys("jennifer@humantech.pe");

            driver.FindElement(By.Id("identifierNext")).Click();

            System.Threading.Thread.Sleep(1000);

            driver.FindElement(By.Name("password")).Click();
            driver.FindElement(By.Name("password")).SendKeys("123456");

            driver.FindElement(By.Id("passwordNext")).Click();

            System.Threading.Thread.Sleep(1000);
        }


    }
}
