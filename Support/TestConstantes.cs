﻿namespace SeleniumHubNUnit.Support
{
    public class TestConstantes
    {
        public const string Standalone = "std";
        public const string BrowserStack = "bst";
        public const string AmazonWs = "aws";


        public const string GoogleChrome = "chrome";
        public const string Firefox = "firefox";
        public const string Safari = "safari";
        public const string InternetExplorer = "iexplore";
        public const string Edge = "edge";

    }
}
